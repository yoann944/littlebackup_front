import { Component, OnInit } from '@angular/core';
import { HttpProvidersService } from '../services/user.service';
import { UserService } from '../services/node/user.service';

@Component({
  selector: 'app-modif-profil',
  templateUrl: './modif-profil.component.html',
  styleUrls: ['./modif-profil.component.css']
})
export class ModifProfilComponent implements OnInit {

  isUser:any;
  userInfos:any = {};
  constructor(private http: HttpProvidersService, private userServ: UserService) 
  {
    this.isUser = localStorage.getItem('currentUser'); 
    this.getUserById();
  }

  ngOnInit() {
  }

  getUserById()
  {

    this.userServ.getUserById(this.isUser).subscribe((res) => {
      console.log(res);
      this.userInfos.firstName = res['firstName'];
      this.userInfos.lastName = res['lastName'];
      this.userInfos.email = res['email'];
      this.userInfos.photo = res['photo'];

    })
  }

  public updateUser()
  {
    this.userInfos.id = this.isUser;

    this.userServ.updateUser(this.userInfos).subscribe((res) => {
      console.log('update user res : ');
      console.log(res);
      location.reload();
    })
  }

  private delteUser(id: string)
  {
    this.userServ.deleteUser(id).subscribe((res) => {
      console.log('delete user id : '+id);
      console.log('response delete user :');
      console.log(res);
    })
  }
}
