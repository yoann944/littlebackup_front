import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { HttpProvidersService } from '../services/user.service';
import { RouterModule, Routes } from '@angular/router';
import { UserService } from '../services/node/user.service';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {

  private isUser: any;
  public user: any;
  public userInfos: any = {};

  constructor(
    private location: Location,
    private http: HttpProvidersService,
    private userService: UserService
  ) {
    // this.isUser = localStorage.getItem('currentUser');
    // console.log(localStorage.getItem('currentUser'));
    // this.getUserByEmail();

    // this.getAllUserNode();
    this.getUserNodeById('5c093c7a4c0e3210f8e9da17');
  }

  ngOnInit() {
  }


  logout() {
    localStorage.removeItem('currentUser');
    console.log(localStorage.getItem('currentUser'));
    location.reload(true);
  }

  // getUserByEmail()
  // {
  //   this.userInfos = this.http.getUserByEmail(this.isUser);
  //   this.userInfos.then(userInfos =>
  //   {
  //     this.userInfos = userInfos;
  //     console.log('user info couche 1');

  //     this.userInfos.email = this.userInfos[0]['email'];
  //     this.userInfos.nom = this.userInfos[0]['nom'];
  //     this.userInfos.prenom = this.userInfos[0]['prenom'];
  //     this.userInfos.time_cours = this.userInfos[0]['time_cours'];
  //     this.userInfos.is_prof = this.userInfos[0]['is_prof'];
  //     this.userInfos.photo = this.userInfos[0]['photo'];

  //     console.log(this.userInfos.email);
  //   })
  // }

  private getUserNodeById(id: any) {
    this.userService.getUserById(id).subscribe((res) => {
      console.log(res);
      this.userInfos.coursesTime = res['coursesTime'];
      this.userInfos.firstName = res['firstName'];
      this.userInfos.lastName = res['lastName'];
      this.userInfos.credit = res['credit'];
      this.userInfos.email = res['email'];
      this.userInfos.photo = res['photo'];
      this.userInfos.role = res['role'];

      localStorage.setItem('currentUser', res['_id'])
    })
  }


  private getAllUserNode() {
    this.userService.getAllUser().subscribe((users) => {
      console.log('all users : ');
      console.log(users);
    });
  }

}
