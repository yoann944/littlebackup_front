import { Component } from '@angular/core';
import { UserService } from './services/node/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  activeHeader: boolean;
  public date: any;
  public firstName: any;
  public coins: any;
  public lastName: any;

  constructor(private userServ: UserService) {
    this.date = new Date().toLocaleDateString();
  }

  disconnect() {
    localStorage.clear();
  }
}


