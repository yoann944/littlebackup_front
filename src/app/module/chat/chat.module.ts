import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MaterialModule } from "../../shared/material/material.module";

import { ChatComponent } from "./chat.component";
import { SocketService } from "./shared/services/socket.service";
import { DialogUserComponent } from "./dialog-user/dialog-user.component";
import { RouterModule, Routes, CanActivate } from "@angular/router";

import { AuthGuardService as AuthGuard } from '../../services/auth-guard.service';

const routes: Routes = [
  {
    path: "chat",
    component: ChatComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [ChatComponent, DialogUserComponent],
  providers: [SocketService],
  entryComponents: [DialogUserComponent]
})
export class ChatModule {}
