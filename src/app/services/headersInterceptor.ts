import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpResponse
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
// import { HttpProvidersService as AuthService } from "./user.service";

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  // constructor(public auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const dummyrequest = req.clone({
      setHeaders: {
        'content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    });
    console.log("Cloned Request");
    console.log(dummyrequest);
    return next.handle(dummyrequest);
  }
}
