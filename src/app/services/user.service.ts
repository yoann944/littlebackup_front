import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { JwtHelper } from "angular2-jwt";
// import { JwtHelperService } from "@auth0/angular-jwt";

import * as settings from "../../../config/settings";

@Injectable()
export class HttpProvidersService {
  linkAPI: string = `${settings.default.apiBase}authentication`;
  urlGetUser: string;

  private jwtHelper: JwtHelper = new JwtHelper();

  constructor(private http: HttpClient) {}

  loginBDD(username, password) {
    console.log("username : " + username + " passwd " + password);
    let object: any = {
      email: username,
      password: password
    };

    return new Promise(resolve => {
      this.http
        .post(this.linkAPI, object)
        .subscribe(res => resolve(res), err => console.error(err));
    });
  }

  getUserByEmail(email) {
    var objet = {
      email: email
    };

    return new Promise(resolve => {
      this.http.post(this.urlGetUser, objet).subscribe(
        res => {
          console.log("success data post");
          console.log(res);
          resolve(res);
        },
        err => {
          console.log("Error occured : ");
          console.log(err);
        }
      );
    });
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem("token");
    // Check whether the token is expired and return
    // true or false
    if (token == null) {
      return false;
    } else {
      return !this.jwtHelper.isTokenExpired(token);
    }
  }

  getToken() {
    return localStorage.getItem("token");
  }
}
