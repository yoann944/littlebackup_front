import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CoursService {

  private url: string = 'http://localhost:3000/cours/';
  constructor(private http: HttpClient) { }

  public getAllCours(): Observable<any>
  {
    return this.http.get(this.url)
  }


  public getCoursById(id: string): Observable<any>
  {
    return this.http.get(this.url+id);
  }

  public createCours(cours: any): Observable<any>
  {
    return this.http.post(this.url, JSON.stringify(cours));
  }

  public updateCours(cours: any): Observable<any>
  {
    return this.http.put(this.url, JSON.stringify(cours));
  }

  public deleteCours(id: string): Observable<any>
  {
    return this.http.delete(this.url+id);
  }

}
