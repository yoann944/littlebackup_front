import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  linkAPI:string;
  urlUser:string;
  urlUsers:string;

  constructor(private http: HttpClient) 
  {
    this.urlUser = "http://localhost:3000/user/";
    this.urlUsers = "http://localhost:3000/users/";
  }

  public getUserById(id): Observable<any>
  {
    return this.http.get(this.urlUser+id);
  }

  public getAllUser(): Observable<any>
  {
    return this.http.get(this.urlUsers);
  }

  public createUser(user: any): Observable<any>
  {
    return this.http.post(this.urlUser, user);
  }

  public updateUser(user): Observable<any>
  {
    return this.http.put(this.urlUser+user.id, user);
  }

  public deleteUser(id: string): Observable<any>
  {
    return this.http.delete(this.urlUser+id);
  }
}
