import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import * as settings from '../../../config/settings';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChatService {
  private url = settings.default.apiBase;
  private socket;

  constructor() {
    this.socket = io(this.url);
  }

  public sendMessage(message) {
    this.socket.emit('new-message', message);
    console.info('socket emitted');
  }

  public getMessages() {
    console.info('service getMessage triggered')
    return Observable.create((observer) => {
      this.socket.on('new-message', (message) => {
        console.info('message create service : ', message);
        observer.next(message);
      });
    });
  }
}
