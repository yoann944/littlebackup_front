import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat.service';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  message: string;
  messages: string[] = [];

  constructor(
    private chatService: ChatService
  ) { }

  sendMessage() {
    console.info('sendMessage triggered')
    this.chatService.sendMessage(this.message);
    this.message = '';
  }

  ngOnInit() {
    this.chatService
      .getMessages()
      .subscribe((message: string) => {
        console.info('messages get component : ', message);
        this.messages.push(message);
      }, error => console.error(error));
      console.info('component oninit triggered')

  }


}
