import { Component, OnInit } from "@angular/core";
import { HttpProvidersService } from "../services/user.service";
import { Location } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: any = {};
  user: any;
  loginError: string;

  constructor(
    private httpProvider: HttpProvidersService,
    private location: Location,
    private router: Router
  ) {
    this.user = localStorage.getItem("currentUser");
  }

  ngOnInit() {}

  logForm() {
    if (this.loginForm.username && this.loginForm.password) {
      this.httpProvider
        .loginBDD(this.loginForm.username, this.loginForm.password)
        .then((result: any) => {
          console.log("promise : ");
          console.log(result);
          if (result.user && result.token) {
            localStorage.setItem("currentUser", result.user);
            localStorage.setItem("token", result.token);
            this.router.navigate(["/dashBoard"]);
          } else {
            this.loginError = "Mauvais identifiant ou mot de passe";
          }
        })
        .catch(err => console.log(err.message));
    } else {
      alert("Email ou mot de passe incomplet");
    }
  }
}
