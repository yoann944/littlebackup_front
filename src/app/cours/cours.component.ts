import { Component, OnInit } from '@angular/core';
import { CoursService } from '../services/node/cours/cours.service';
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.css']
})
export class CoursComponent implements OnInit {

  public lesCours: any;

  constructor(private coursServ: CoursService, private sanitizer: DomSanitizer) {
    this.coursServ.getAllCours().subscribe((cours) => {
      console.log(cours);
      cours.forEach(element => {
        element.urlVideo = sanitizer.bypassSecurityTrustHtml(element.urlVideo);
      });

      this.lesCours = cours;
    });

  }

  ngOnInit() {
  }
}
