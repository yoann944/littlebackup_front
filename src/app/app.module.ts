import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { RouterModule, Routes, CanActivate } from "@angular/router";

import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpProvidersService } from "./services/user.service";
import { UserService } from "./services/node/user.service";
import { CoursService } from "./services/node/cours/cours.service";

import { AppComponent } from "./app.component";
import { DashBoardComponent } from "./dash-board/dash-board.component";
import { StreamBroardComponent } from "./stream-broard/stream-broard.component";
import { DrawBoardComponent } from "./draw-board/draw-board";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { CalendarComponent } from "./calendar/calendar.component";
import { ReplayComponent } from "./replay/replay.component";
import { ModifProfilComponent } from "./modif-profil/modif-profil.component";
import { CoursComponent } from "./cours/cours.component";
import { SubscribeComponent } from "./subscribe/subscribe.component";
import { ChatService } from "./services/chat.service";
import { ChatModule } from "./module/chat/chat.module";
import { SharedModule } from "./shared/shared.module";

import { AuthGuardService as AuthGuard } from "./services/auth-guard.service";
import { Twitch } from "./services/twitch/twitch.service";
import { Safe } from "../pipes/safe.pipe";
import { HttpModule } from "@angular/http";

import { HeaderInterceptor } from "./services/headersInterceptor";

const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  {
    path: "dashBoard",
    component: DashBoardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "streamBoard",
    component: StreamBroardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "drawBoard",
    component: DrawBoardComponent,
    canActivate: [AuthGuard]
  },
  { path: "register", component: RegisterComponent, canActivate: [AuthGuard] },
  { path: "calendar", component: CalendarComponent, canActivate: [AuthGuard] },
  { path: "replay", component: ReplayComponent, canActivate: [AuthGuard] },
  {
    path: "modifProfil",
    component: ModifProfilComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "subscribe",
    component: SubscribeComponent,
    canActivate: [AuthGuard]
  },
  { path: "cours", component: CoursComponent, canActivate: [AuthGuard] },
  { path: "", redirectTo: "login", pathMatch: "full" }
];

@NgModule({
  declarations: [
    AppComponent,
    DashBoardComponent,
    StreamBroardComponent,
    DrawBoardComponent,
    LoginComponent,
    RegisterComponent,
    CalendarComponent,
    ReplayComponent,
    ModifProfilComponent,
    CoursComponent,
    SubscribeComponent,
    Safe
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ChatModule,
    SharedModule,
    HttpModule
  ],
  providers: [
    Twitch,
    HttpProvidersService,
    UserService,
    CoursService,
    ChatService,
    AuthGuard,
    HttpModule,
    { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
