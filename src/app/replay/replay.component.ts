import { Component, OnInit } from '@angular/core';
import { CoursService } from '../services/node/cours/cours.service';
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-replay',
  templateUrl: './replay.component.html',
  styleUrls: ['./replay.component.css']
})
export class ReplayComponent implements OnInit {

  public replays: any;

  constructor(private coursService: CoursService, private sanitizer: DomSanitizer) 
  {
    this.coursService.getAllCours().subscribe((cours) => {
      console.log(cours);
      cours.forEach(element => {
        element.urlVideo = sanitizer.bypassSecurityTrustHtml(element.urlVideo);
      });

      this.replays = cours
    })
  }

  ngOnInit() {
  }




}
